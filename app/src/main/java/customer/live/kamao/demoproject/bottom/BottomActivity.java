package customer.live.kamao.demoproject.bottom;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import customer.live.kamao.demoproject.R;

public class BottomActivity extends AppCompatActivity {

    ViewPager viewPager;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        initView();
    }

    private void initView(){
        bottomNavigationView = (BottomNavigationView)findViewById(R.id.navigation);
        viewPager = (ViewPager)findViewById(R.id.viewPager);
        setupAdapter();
        onItemSelection();
    }

    private void onItemSelection(){
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.action_item1 :
                        viewPager.setCurrentItem(0);
                        break;

                    case R.id.action_item2 :
                        viewPager.setCurrentItem(1);
                        break;

                    case R.id.action_item3 :
                        viewPager.setCurrentItem(2);
                        break;

                    case R.id.action_item4 :
                        viewPager.setCurrentItem(3);
                        break;

                    case R.id.action_item5 :
                        viewPager.setCurrentItem(4);
                        break;

                }
                return true;
            }
        });
    }

    private void setupAdapter(){
        BottomViewAdapter bottomViewAdapter = new BottomViewAdapter(getSupportFragmentManager());
        viewPager.setAdapter(bottomViewAdapter);

    }

    public static class BottomViewAdapter extends FragmentStatePagerAdapter {

        private BottomViewAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment selectedFragment = null;
            switch (position) {
                case 0: {
                    selectedFragment = ItemOneFragment.newInstance();
                    break;
                }
                case 1: {
                    selectedFragment = ItemTwoFragment.newInstance();
                    break;
                }
                case 2: {
                    selectedFragment = ItemThreeFragment.newInstance();
                    break;
                }
                case 3: {
                    selectedFragment = ItemFourFragment.newInstance();
                    break;
                }
                case 4: {
                    selectedFragment = ItemFiveFragment.newInstance();
                    break;
                }
            }
            return selectedFragment;
        }

        @Override
        public int getCount() {
            return 5;
        }
    }
}
