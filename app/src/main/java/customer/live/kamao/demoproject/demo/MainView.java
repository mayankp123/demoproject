package customer.live.kamao.demoproject.demo;

public interface MainView {

    void showProgress();

    void hideProgress();

    void setQuote(String string);
}
