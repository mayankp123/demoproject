package customer.live.kamao.demoproject.bottom;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import customer.live.kamao.demoproject.R;

public class ItemFiveFragment extends Fragment {

    public static ItemFiveFragment newInstance(){
        ItemFiveFragment fragment = new ItemFiveFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item_five, container, false);
    }
}
