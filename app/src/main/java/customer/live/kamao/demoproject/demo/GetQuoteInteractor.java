package customer.live.kamao.demoproject.demo;

public interface GetQuoteInteractor {

    interface OnFinishedListener {
        void onFinished(String string);
    }

    void getNextQuote(OnFinishedListener onFinishedListener);

}
